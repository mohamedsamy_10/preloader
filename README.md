# Page Preloader
Easy way to make a preloader for your app

## installation

    composer require mosamy/preloader

Publish the package assets

    php artisan vendor:publish --provider="Mosamy\Preloader\PreloaderServiceProvider" --tag="assets"

## Usage

include the page assets in every page you will use the preloader

```html
<!DOCTYPE html>
<html lang="ar" dir="rtl">
  <head>
    <meta charset="utf-8">
    ...
    @PreloaderStyle
  </head>

  <body>
    <x-preloader::init />

	...

    @PreloaderJs
  </body>
 </html>
```

> Thats it!
