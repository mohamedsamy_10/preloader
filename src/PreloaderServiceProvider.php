<?php

namespace Mosamy\Preloader;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;

class PreloaderServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {

    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {

      Blade::directive('PreloaderJs', function () {
          return '<script src="'.asset('js/preloader.js').'"></script>';
      });

      Blade::directive('PreloaderStyle', function () {
          return '<link rel="stylesheet" href="'.asset('css/preloader.css').'" />';
      });

      //php artisan vendor:publish --provider="Mosamy\Preloader\PreloaderServiceProvider" --tag="assets"
      $this->publishes([
        __DIR__.'/js/preloader.js' => public_path('js/preloader.js'),
        __DIR__.'/css/preloader.css' => public_path('css/preloader.css'),
        __DIR__.'/images/preloader.svg' => public_path('images/preloader.svg'),
      ], 'assets');

      $this->loadViewsFrom(__DIR__ . '/views', 'preloader');
    }
}
